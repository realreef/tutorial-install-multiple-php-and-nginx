# Step 1: Installing and Enabling EPEL and Remi Repository

1\. First start by installing and enabling the **EPEL** and **Remi** repository, which offers the latest versions of the **PHP** stack on **CentOS/RHEL 7** distributions.
```sh
# yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
# yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
```

2\. Next install the **yum-utils** package, which extends yum’s native functionalities and provides **yum-config-manager** command, which is used to enable or disable Yum repositories on the system.
```sh
# yum install yum-utils
```
**`Note`**: On **RHEL 7** you can enable the optional channel for some dependencies using the following command.
```sh
# subscription-manager repos --enable=rhel-7-server-optional-rpms
```

# Step 2: Installing Nginx Web Server

3\. To install latest version of Nginx, we need to add the official Nginx repository, create a file named **/etc/yum.repos.d/nginx.repo**
```sh
# vim /etc/yum.repos.d/nginx.repo
```
Add the following lines to file as per your distribution.
```sh
--------------- On CentOS 7 ---------------
[nginx] 
name=nginx repo 
baseurl=http://nginx.org/packages/centos/7/$basearch/ 
gpgcheck=0 
enabled=1

--------------- On RHEL 7 ---------------
[nginx] 
name=nginx repo 
baseurl=http://nginx.org/packages/rhel/7.x/$basearch/ 
gpgcheck=0 
enabled=1
```

4\. Once nginx repo has been added, you can install Nginx using yum package manager tool as shown.
```sh
# yum install nginx
```

# Step 3: Installing Multiple Versions of PHP

4\. To install different versions of PHP for your projects, use **yum-config-manager** command to install multiple versions of PHP along with most required modules as shown.
#### `Install PHP 7.2 Version`
```sh
# yum-config-manager --enable remi-php72               [default]
# yum install php php-common php-fpm
# yum install php-mysql php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml php-pecl-apc php-cli php-pear php-pdo
```

### `Install PHP 5.6 Version`
```sh
# yum install php56 php56-php-common php56-php-fpm
# yum install php56-php-mysql php56-php-pecl-memcache php56-php-pecl-memcached php56-php-gd php56-php-mbstring php56-php-mcrypt php56-php-xml php56-php-pecl-apc php56-php-cli php56-php-pear php56-php-pdo
```

5\. Once installed PHP, you can use following command to check the default version of PHP used on your server.
```sh
# php -v
```

# Step 4: Configuring PHP-FPM and PHP56-PHP-FPM

5\. This is the most interesting part of this tutorial, it explains how you can actually run multiple PHP versions on your server. Here, you will configure the different versions of **php-fpm** that Nginx will work with. You should define the user/group of the FastCGI processes as well as the ports they will listen on.

These are the following two configuration files that you will going to edit.
*  php-fpm (default 7.2) –-> /etc/php-fpm.d/www.conf
* php56-php-fpm –-> /opt/remi/php56/root/etc/php-fpm.d/www.conf

Open the files above, set the user/group of FastCGI processes.
```sh
# vim /etc/php-fpm.d/www.conf   [PHP 7.2]
# vim /opt/remi/php56/root/etc/php-fpm.d/www.conf  [PHP 5.6]
```

The default values should be **apache**, change them to **nginx** as shown.
```sh
user = nginx
group = nginx
```

6\. Next, find the listen parameters, and define the **address:port** on which FastCGI requests will be received.
```sh
listen = 127.0.0.1:9072	[php-fpm]
listen = 127.0.0.1:9056	[php56-php-fpm]
```

7\. Once all the above configuration done, you need to start and enable **Nginx** and **PHP-FPM** to auto-start at system boot.
```sh
systemctl enable nginx 
systemctl start nginx

---------------- PHP 7.2 ---------------- 
systemctl enable php-fpm 
systemctl start php-fpm 

---------------- PHP 5.6 ----------------
systemctl enable php56-fpm 
systemctl start php56-php-fpm
```

**`Attention`**: In case you get any errors while starting the second instance of PHP, **php56-php-fpm**, a SELinux policy could be blocking it from starting. If SELinux is in **enforcing mode**, set it to **permissive mode**, then try starting the service once again.
```sh
getenforce
setenforce 0
```

8\. for install **laravel v5.7**, that php version must be installed extension **bcmath**.
```sh
# yum install php72-php-bcmath.x86_64
# cp /etc/opt/remi/php72/php.d/20-bcmath.ini /etc/php.d/
# cp /opt/remi/php72/root/usr/lib64/php/modules/bcmath.so /usr/lib64/php/modules/
# service nginx restart
```